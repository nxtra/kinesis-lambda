# Kinesis

You can deploy the infrastructure of this tutorial using the aws cli or cloudformation.
Both are explained below.

## HowTo CLI

### IAM
Create role lambda-kinesis-role and attach policy:  
`{
     "Version": "2012-10-17",
     "Statement": [
         {
             "Effect": "Allow",
             "Action": [
                 "kinesis:DescribeStream",
                 "kinesis:GetRecords",
                 "kinesis:GetShardIterator",
                 "kinesis:ListStreams",
                 "logs:CreateLogGroup",
                 "logs:CreateLogStream",
                 "logs:PutLogEvents"
             ],
             "Resource": "*"
         }
     ]
 }`
 
### Lambda
Create function: `aws lambda create-function --function-name ProcessKinesisRecords --zip-file fileb://./target/kinesis-1.0-SNAPSHOT.jar --handler ProcessKinesisRecords::handleRequest --runtime java8 --role arn:aws:iam::389795768041:role/lambda-kinesis-role`  
Test function and save output in out.txt: `aws lambda invoke --invocation-type RequestResponse --function-name ProcessKinesisRecords --payload file://input.txt out.txt`  
The Data in the test event is the 256 hash of the String `Hello, this is a test 123`   
Update a function: `aws lambda update-function-code --function-name ProcessKinesisRecords --zip-file fileb://./target/kinesis-1.0-SNAPSHOT.jar`  
Delete function: `aws lambda delete-function --function-name ProcessKinesisRecords`


### Kinesis
Create stream: `aws kinesis create-stream --stream-name kinesis-stream --shard-count 1`  
Retrieve stream arn: `aws kinesis describe-stream --stream-name kinesis-stream`  
Create event source for lambda:  
  `aws lambda create-event-source-mapping --function-name ProcessKinesisRecords \
 --event-source  arn:aws:kinesis:eu-west-1:389795768041:stream/kinesis-stream \
 --batch-size 100 --starting-position TRIM_HORIZON`  
 Get list of event-source-mappings and check if enabled:  
 `aws lambda list-event-source-mappings --function-name ProcessKinesisRecords \
  --event-source arn:aws:kinesis:eu-west-1:389795768041:stream/kinesis-stream`  
Put record on the stream:  
`aws kinesis put-record --stream-name kinesis-stream \
 --data "SGVsbG8sIHRoaXMgaXMgYSB0ZXN0Lg==" --partition-key shardId-000000000000`  
Put the record on the stream multiple times:  
`


## HowTo SAM
### Create bucket
Create an s3 bucket to store the deployment file: `aws s3 mb s3://kinesis-tutorial`  
Delete s3 bucket: `aws s3 rb s3://kinesis-tutorial --force` or `aws s3api delete-bucket --bucket kinesis-tutorial`  

### Package 
`aws cloudformation package --template-file ./template.yml --s3-bucket kinesis-tutorial --output-template-file packaged-template.yml`

This will package and store the package on an s3 bucket in your account.
You can specify a specific region other than your default with `aws --region region cloudformation ..`
It will generate a packaged-template.yml file with a new CodeUri. 
This CodeUri points to the zip file of your project on S3.  
You can download this zip file with `aws s3 cp s3://kinesis-tutorial/md5hashons3 localname`

### Deploy
`aws cloudformation deploy --template-file ./packaged-template.yml --stack-name kinesis-client-tutorial --capabilities CAPABILITY_IAM`

This will deploy your stack. 
It will be visible in the CloudFormation tab of the AWS Console

### Delete
`aws cloudformation delete-stack --stack-name kinesis-client-tutorial`

Delete the stack and remove it from your aws account.
It will no longer be visible in the CloudFormation tab.

### Retrieving a template
`aws cloudformation get-template --stack-name kinesis-client-tutorial`

### Convert yaml <> json
json to yaml: 
`ruby -ryaml -rjson -e 'puts YAML.dump(JSON.load(ARGF))' < template.json > template.yml`

yaml to json: 
`ruby -rjson -ryaml -e "puts JSON.pretty_generate(YAML.load(ARGF))" < template.yml > template.json`

