import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.KinesisEvent;

import java.net.InetAddress;
import java.util.UUID;

public class ProcessKinesisRecords implements RequestHandler<KinesisEvent, Void> {

    public static final String SAMPLE_APPLICATION_STREAM_NAME = "kinesis-stream";
    public static final String SAMPLE_APPLICATION_STREAM_REGION = "eu-west-1";
    private static final String SAMPLE_APPLICATION_NAME = "SampleKinesisLambdaApplication";

    // Initial position in the stream when the application starts up for the first time.
    // Position can be one of LATEST (most recent data) or TRIM_HORIZON (oldest available data)
    // When checkpointing it will automatically start from latest checkpoint even though position is set
    private static final InitialPositionInStream SAMPLE_APPLICATION_INITIAL_POSITION_IN_STREAM =
            InitialPositionInStream.LATEST;

    private static EnvironmentVariableCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider();

    public Void handleRequest(KinesisEvent event, Context context) {
//        init();
        int exitCode = 0;

        try {
            String workerId = InetAddress.getLocalHost().getCanonicalHostName() + ":" + UUID.randomUUID();
            KinesisClientLibConfiguration kinesisClientLibConfiguration =
                    new KinesisClientLibConfiguration(SAMPLE_APPLICATION_NAME,
                            SAMPLE_APPLICATION_STREAM_NAME,
                            credentialsProvider,
                            workerId);
            kinesisClientLibConfiguration.withInitialPositionInStream(SAMPLE_APPLICATION_INITIAL_POSITION_IN_STREAM);
            kinesisClientLibConfiguration.withRegionName(SAMPLE_APPLICATION_STREAM_REGION);

            IRecordProcessorFactory recordProcessorFactory = new AmazonKinesisApplicationRecordProcessorFactory();
            Worker worker = new Worker(recordProcessorFactory, kinesisClientLibConfiguration);

            System.out.printf("Running %s to process stream %s as worker %s...\n",
                    SAMPLE_APPLICATION_NAME,
                    SAMPLE_APPLICATION_STREAM_NAME,
                    workerId);
            worker.run();
        } catch (Throwable e) {
            System.err.println("Caught throwable while processing data.");
            e.printStackTrace();
        }
        System.exit(exitCode);
        return null;
    }


//    private static void init() {
//        // Ensure the JVM will refresh the cached IP values of AWS resources (e.g. service endpoints).
//        java.security.Security.setProperty("networkaddress.cache.ttl", "60");
//
//        credentialsProvider = new ProfileCredentialsProvider();
//
//        try {
//            credentialsProvider.getCredentials();
//        } catch (Exception e) {
//            throw new AmazonClientException("Cannot load the credentials", e);
//        }
//    }

}